require "sinatra"
require "endpoint_base"
require "sinatra/reloader"

require_all 'lib'

class OpencartIntegration < EndpointBase::Sinatra::Base
  # EndpointBase adds the following instance variables
  # @config which stores only "parameters" portion of an incoming request
  # @payload which seems to store the entire incoming request

  # Force Sinatra to autoreload this file or any file in the lib directory when they change in development
  configure :development do
    register Sinatra::Reloader
    also_reload './lib/**/*'
  end

  # Supported endpoints:
  # get_ for orders and customers
  post '/get_orders' do
    begin
      # Return an order object.
      oc_api = new_opencart_api_request(@payload,@config)
      since = Time.now.utc.iso8601
      orders = oc_api.get_orders_since(@config['since'])
      orders.each do |o|
        add_object :order, o.as_wombat_hash
      end
      add_parameter 'since', since

      # Return the relevant HTTP status code, and set the notification summary.
      # Conditional avoids "Successfully retrieved 0 order(s)." notifications.
      if orders.size == 0
        return result 200
      else
        return result 200, "Successfully retrieved " + orders.size.to_s + " order(s)."
      end
    rescue => e
      result 500, (e.try(:response) ? e.response : e.message)
    end
  end

  private
    def new_opencart_api_request(payload,config)
      host_url = @config[:opencart_host]
      api_key = @config[:opencart_apikey]
      request_id = @payload[:request_id]

      ##TODO Raise error if critical values for an opencart api request are missing
      ## Could do this in the Api class instead

      Opencart::Api.new(host_url, api_key, request_id, payload, config)
    end

end
