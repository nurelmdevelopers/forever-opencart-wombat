# OpenCart Integration

## Overview

## Developer Environment Setup
Perform the following commands to setup your development environment:
```sh
$ cp Vagrantfile.default Vagrantfile
## Make any preference changes in Vagrantfile in your prefered editor
$ vagrant up
$ vagrant ssh
$ cd /vagrant
$ gem install bundler
$ bundle install
$ bundle exec rackup
```

## Connection Parameters

The following parameters must be setup within [Wombat](http://wombat.co):

| Name | Value |
| :----| :-----|
| opencart_apikey   | OpenCart account API key (required) |
| opencart_host     | OpenCart account host, no 'http://' (required) |

## Webhooks

The following webhooks are implemented. For all 'get_' webhooks, a
'opencart_id' field is return that is used to tie a Wombat object to its
corresponding OpenCart object.

* **get_orders**: Retrieves all orders since the last time a wombat retrieved orders.

## Notes