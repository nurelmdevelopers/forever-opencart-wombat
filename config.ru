require 'rubygems'
require 'bundler'

Bundler.require(:default)
require "./opencart_integration"
run OpencartIntegration
