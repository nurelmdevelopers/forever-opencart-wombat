class LineItem

  attr_reader :sku

  def add_opencart_obj opencart_li, opencart_api
    @opencart_id = opencart_li['id']
    @opencart_parent_id = opencart_li['product_id']
    @sku = opencart_li['sku']
    @name = opencart_li['name']
    @quantity = opencart_li['quantity'].to_i
    @price = opencart_li['price'].to_f

    self
  end

  def wombat_obj
    [
      {
        'product_id' => @sku,
        'opencart_id' => @opencart_id.to_s,
        'opencart_parent_id' => @opencart_parent_id.to_s,
        'name' => @name,
        'quantity' => @quantity,
        'price' => @price
      }
    ]
  end

end
