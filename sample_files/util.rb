class Util

  def self.wombat_array objs
    wombat_array = Array.new
    objs.each do |obj|
      wombat_obj = obj.wombat_obj
      if wombat_obj.kind_of?(Array)
        wombat_array += obj.wombat_obj
      else
        wombat_array << obj.wombat_obj
      end
    end
    wombat_array
  end

  def self.opencart_array objs
    opencart_array = Array.new
    objs.each do |obj|
      opencart_array << obj.opencart_obj
    end
    opencart_array
  end

  def self.wombat_shipment_status opencart_status
    (opencart_status == 'success') ? 'shipped' : 'ready'
  end

  def self.opencart_shipment_status wombat_status
    opencart_status = 'error'

    case wombat_status
    when 'shipped'
      opencart_status = 'success'
    when 'ready'
      opencart_status = 'pending'
    else
      opencart_status = 'failure'
    end

    opencart_status
  end

  def self.opencart_apikey wombat_config
    wombat_config['opencart_apikey']
  end

  def self.opencart_password wombat_config
    wombat_config['opencart_password']
  end

  def self.opencart_host wombat_config
    wombat_config['opencart_host']
  end

end
