class Product

  attr_reader :opencart_id, :variants

  def add_opencart_obj opencart_product, opencart_api
    @opencart_id = opencart_product['id']
    @source = Util.opencart_host opencart_api.config
    @name = opencart_product['title']
    @description = opencart_product['body_html']

    @options = Array.new
    unless opencart_product['options'].nil?
      opencart_product['options'].each do |opencart_option|
        option = Option.new
        option.add_opencart_obj opencart_option
        @options << option
      end
    end

    @variants = Array.new
    unless opencart_product['variants'].nil?
      opencart_product['variants'].each do |opencart_variant|
        variant = Variant.new
        variant.add_opencart_obj opencart_variant, opencart_product['options']
        @variants << variant
      end
    end

    @images = Array.new
    unless opencart_product['images'].nil?
      opencart_product['images'].each do |opencart_image|
        image = Image.new
        image.add_opencart_obj opencart_image
        @images << image
      end
    end

    self
  end

  def add_wombat_obj wombat_product, opencart_api
    @opencart_id = wombat_product['opencart_id']
    @wombat_id = wombat_product['id'].to_s
    @name = wombat_product['name']
    @description = wombat_product['description']

    @options = Array.new
    unless wombat_product['options'].blank?
      wombat_product['options'].each do |wombat_option|
        option = Option.new
        option.add_wombat_obj wombat_option
        @options << option
      end
    else
      option = Option.new
      option.add_wombat_obj 'Default'
      @options << option
    end

    @variants = Array.new
    unless wombat_product['variants'].nil?
      wombat_product['variants'].each do |wombat_variant|
        variant = Variant.new
        variant.add_wombat_obj wombat_variant
        @variants << variant
      end
    end

    @images = Array.new
    unless wombat_product['images'].nil?
      wombat_product['images'].each do |wombat_image|
        image = Image.new
        image.add_wombat_obj wombat_image
        @images << image
      end
    end
    @variants.each do |variant|
      variant.images.each do |image|
        @images << image
      end
    end

    self
  end

  def wombat_obj
    {
      'id' => @opencart_id.to_s,
      'opencart_id' => @opencart_id.to_s,
      'source' => @source,
      'name' => @name,
      'sku' => @name,
      'description' => @description,
      'meta_description' => @description,
      'options' => Util.wombat_array(@options),
      'variants' => Util.wombat_array(@variants),
      'images' => Util.wombat_array(@images)
    }
  end

  def opencart_obj
    {
      'product'=> {
        'title'=> @name,
        'body_html'=> @description,
        'product_type' => 'None',
        'options' => Util.opencart_array(@options),
        'variants'=> Util.opencart_array(@variants),
        'images' => Util.opencart_array(@images)
      }
    }
  end

  def opencart_obj_no_variants
    obj_no_variants = opencart_obj
    obj_no_variants['product'].delete('variants')
    obj_no_variants
  end

end
