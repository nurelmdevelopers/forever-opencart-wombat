require 'json'
require 'rest-client'
require 'pp'

class OpencartAPI
  attr_accessor :order, :config, :payload, :request

  def initialize payload, config={}
    @payload = payload
    @config = config
  end

  def get_inventory
    inventories = Array.new
    get_objs('products', Product).each do |product|
      unless product.variants.nil?
        product.variants.each do |variant|
          unless variant.sku.blank?
            inventory = Inventory.new
            inventory.add_obj variant
            inventories << inventory.wombat_obj
          end
        end
      end
    end
    get_reply inventories, "Retrieved inventories."
  end

  def get_shipments
    shipments = Array.new
    get_objs('orders', Order).each do |order|
      shipments += shipments(order.opencart_id)
    end
    get_webhook_results 'shipments', shipments, false
  end

  def add_shipment

  end

  def update_shipment
    shipment = Shipment.new.add_wombat_obj @payload['shipment'], self
    wombat_status = @payload['shipment']['status']
    unless (wombat_status == 'ready' || shipment.opencart_id.nil?)
      ## If Opencart ID exists, update shipment
      result = api_put "orders/#{shipment.opencart_order_id}/" +
                       "fulfillments/#{shipment.opencart_id}.json",
                       {'fulfillment' => shipment.opencart_obj}
      response = {
        'objects' => result,
        'message' => "Updated shipment for order with Opencart ID of " +
                     "#{shipment.opencart_order_id}."
      }
    else
      response = {
        'message' => nil
      }
    end

    response
  end

  def add_metafield obj_name, opencart_id, wombat_id
    api_post obj_name + 's/' + opencart_id +  '/metafields.json',
             Metafield.new(@payload[obj_name]['id']).opencart_obj
  end

  def wombat_id_metafield obj_name, opencart_id
    wombat_id = nil
    metafields_array = api_get obj_name + 's/' + opencart_id + '/metafields'
    unless metafields_array.nil? || metafields_array['metafields'].nil?
      metafields_array['metafields'].each do |metafield|
        if metafield['key'] == 'wombat_id'
          wombat_id = metafield['value']
          break
        end
      end
    end

    wombat_id
  end

  def shipments order_id
    get_objs "orders/#{order_id}/fulfillments", Shipment
  end


  private

  def get_webhook_results obj_name, obj, get_objs = true
    objs = Util.wombat_array(get_objs ? get_objs(obj_name, obj) : obj)
    get_reply objs, "Successfully retrieved #{objs.length} #{obj_name} " +
                    "from Opencart."
  end

  def get_reply objs, message
    {
      'objects' => objs,
      'message' => message
    }
  end

  def get_objs objs_name, obj_class
    objs = Array.new
    opencart_objs = api_get objs_name
    if opencart_objs.values.first.kind_of?(Array)
      opencart_objs.values.first.each do |opencart_obj|
        obj = obj_class.new
        obj.add_opencart_obj opencart_obj, self
        objs << obj
      end
    else
      obj = obj_class.new
      obj.add_opencart_obj opencart_objs.values.first, self
      objs << obj
    end

    objs
  end

  def find_opencart_id_by_sku sku
    count = (api_get 'products/count')['count']
    page_size = 250
    pages = (count / page_size.to_f).ceil
    current_page = 1

    while current_page <= pages do
      products = api_get 'products',
                         {'limit' => page_size, 'page' => current_page}
      current_page += 1
      products['products'].each do |product|
        product['variants'].each do |variant|
          return variant['id'].to_s if variant['sku'] == sku
        end
      end
    end

    return nil
  end

  def api_get resource, data = {}
    params = ''
    unless data.empty?
      params = '?'
      data.each do |key, value|
        params += '&' unless params == '?'
        params += "#{key}=#{value}"
      end
    end

    response = RestClient.get opencart_url + (final_resource resource) + params
    JSON.parse response.force_encoding("utf-8")
  end

  def api_post resource, data
    response = RestClient.post opencart_url + resource, data.to_json,
                               :content_type => :json, :accept => :json
    JSON.parse response.force_encoding("utf-8")
  end

  def api_put resource, data
    response = RestClient.put opencart_url + resource, data.to_json,
                              :content_type => :json, :accept => :json
    JSON.parse response.force_encoding("utf-8")
  end

  def opencart_url
    "https://#{Util.opencart_apikey @config}:#{Util.opencart_password @config}" +
    "@#{Util.opencart_host @config}/admin/"
  end

  def final_resource resource
    if !@config['since'].nil?
      resource += ".json?updated_at_min=#{@config['since']}"
    elsif !@config['id'].nil?
      resource += "/#{@config['id']}.json"
    else
      resource += '.json'
    end
    resource
  end

end

class AuthenticationError < StandardError; end
class OpencartError < StandardError; end
