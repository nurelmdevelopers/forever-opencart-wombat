class Order

  attr_reader :opencart_id, :email, :shipping_address, :billing_address

  def add_opencart_obj opencart_order, opencart_api
    @store_name = Util.opencart_host(opencart_api.config).split('.')[0]
    @order_number = opencart_order['order_number']
    @opencart_id = opencart_order['id']
    @source = Util.opencart_host opencart_api.config
    @status = 'completed'
    @email = opencart_order['email']
    @currency = opencart_order['currency']
    @placed_on = opencart_order['created_at']
    @totals_item = opencart_order['total_line_items_price'].to_f
    @totals_tax = opencart_order['total_tax'].to_f
    @totals_discounts = opencart_order['total_discounts'].to_f
    @totals_shipping = 0.00
    opencart_order['shipping_lines'].each do |shipping_line|
      @totals_shipping += shipping_line['price'].to_f
    end
    @payments = Array.new
    @totals_payment = 0.00
    opencart_api.transactions(@opencart_id).each do |transaction|
      if transaction.kind == 'capture' and transaction.status == 'success'
        @totals_payment += transaction.amount.to_f
        payment = Payment.new
        @payments << payment.add_opencart_obj(transaction, opencart_api)
      end
    end
    @totals_order = opencart_order['total_price'].to_f
    @line_items = Array.new
    opencart_order['line_items'].each do |opencart_li|
      line_item = LineItem.new
      @line_items << line_item.add_opencart_obj(opencart_li, opencart_api)
    end

    unless opencart_order['shipping_address'].nil?
      @shipping_address = {
        'firstname' => opencart_order['shipping_address']['first_name'],
        'lastname' => opencart_order['shipping_address']['last_name'],
        'address1' => opencart_order['shipping_address']['address1'],
        'address2' => opencart_order['shipping_address']['address2'],
        'zipcode' => opencart_order['shipping_address']['zip'],
        'city' => opencart_order['shipping_address']['city'],
        'state' => opencart_order['shipping_address']['province'],
        'country' => opencart_order['shipping_address']['country_code'],
        'phone' => opencart_order['shipping_address']['phone']
      }
    end

    unless opencart_order['billing_address'].nil?
      @billing_address = {
        'firstname' => opencart_order['billing_address']['first_name'],
        'lastname' => opencart_order['billing_address']['last_name'],
        'address1' => opencart_order['billing_address']['address1'],
        'address2' => opencart_order['billing_address']['address2'],
        'zipcode' => opencart_order['billing_address']['zip'],
        'city' => opencart_order['billing_address']['city'],
        'state' => opencart_order['billing_address']['province'],
        'country' => opencart_order['billing_address']['country_code'],
        'phone' => opencart_order['billing_address']['phone']
      }
    end

    self
  end

  def wombat_obj
    {
      'id' => @store_name.upcase + '-' + @order_number.to_s,
      'opencart_id' => @opencart_id.to_s,
      'source' => @source,
      'channel' => @source,
      'status' => @status,
      'email' => @email,
      'currency' => @currency,
      'placed_on' => @placed_on,
      'totals' => {
        'item' => @totals_item,
        'tax' => @totals_tax,
        'shipping' => @totals_shipping,
        'payment' => @totals_payment,
        'order' => @totals_order
      },
      'line_items' => Util.wombat_array(@line_items),
      'adjustments' => [
        {
          'name' => 'Tax',
          'value' => @totals_tax
        },
        {
          'name' => 'Shipping',
          'value' => @totals_shipping
        },
        {
          'name' => 'Discounts',
          'value' => @totals_discounts
        }
      ],
      'shipping_address' => @shipping_address,
      'billing_address' => @billing_address,
      'payments' => Util.wombat_array(@payments)
    }
  end

end
