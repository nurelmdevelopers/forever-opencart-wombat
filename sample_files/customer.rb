class Customer

  attr_reader :opencart_id

  def add_opencart_obj opencart_customer, opencart_api
    @opencart_id = opencart_customer['id']
    @firstname = opencart_customer['first_name']
    @lastname = opencart_customer['last_name']
    @email = opencart_customer['email']
    @default_address = Address.new.add_opencart_obj(opencart_customer['default_address'])
    @source = Util.opencart_host opencart_api.config
  end

  def add_wombat_obj wombat_customer, shopfiy_api
    @opencart_id = wombat_customer['opencart_id']
    @firstname = wombat_customer['firstname']
    @lastname = wombat_customer['lastname']
    @email = wombat_customer['email']
    @shipping_address = Address.new.add_wombat_obj(wombat_customer['shipping_address'])
    @billing_address = Address.new.add_wombat_obj(wombat_customer['billing_address'])
  end

  def wombat_obj
    {
      'id' => @opencart_id.to_s,
      'opencart_id' => @opencart_id.to_s,
      'source' => @source,
      'firstname' => @firstname,
      'lastname' => @lastname,
      'email' => @email,
      'shipping_address' => @default_address.wombat_obj,
      'billing_address' => @default_address.wombat_obj
    }
  end

  def opencart_obj
    {
      'customer' => {
        'first_name' => @firstname,
        'last_name' => @lastname,
        'email' => @email,
        'addresses' => [
          @shipping_address.opencart_obj,
          @billing_address.opencart_obj
        ]
      }
    }
  end

end
