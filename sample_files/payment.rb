class Payment

  def add_opencart_obj opencart_transaction, opencart_api
    @amount = opencart_transaction.amount
    @payment_method = opencart_transaction.gateway

    self
  end

  def wombat_obj
    {
      'status' => 'completed',
      'amount' => @amount,
      'payment_method' => @payment_method
    }
  end

end