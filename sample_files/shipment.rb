class Shipment

  attr_reader :id, :opencart_id, :opencart_order_id, :status

  def add_opencart_obj opencart_shipment, opencart_api
    @opencart_id = opencart_shipment['id']
    @opencart_order_id = opencart_shipment['order_id']
    @source = Util.opencart_host opencart_api.config
    order = opencart_api.order(@opencart_order_id).first
    @email = order.email
    @status = Util.wombat_shipment_status opencart_shipment['status']
    @shipping_method = (opencart_shipment['tracking_company'] || 'tracking company not set') + ' / ' +
                       (opencart_shipment['service'] || 'service not set')
    @tracking = opencart_shipment['tracking_number']
    @shipped_at = opencart_shipment['created_at']
    @line_items = Array.new
    opencart_shipment['line_items'].each do |opencart_li|
      line_item = LineItem.new
      line_item.add_opencart_obj(opencart_li, opencart_api)
      @line_items << line_item.add_opencart_obj(opencart_li, opencart_api)
    end
    @shipping_address = order.shipping_address
    @billing_address = order.billing_address

    self
  end

  def add_wombat_obj wombat_shipment, opencart_api
    @opencart_order_id = wombat_shipment['id']
    @status = Util.opencart_shipment_status wombat_shipment['status']
    @shipping_method = wombat_shipment['shipping_method']
    @tracking_number = wombat_shipment['tracking']

    self
  end

  def opencart_obj
    {
      'status' => @status,
      'tracking_company' => @shipping_method,
      'tracking_number' => @tracking_number,
    }
  end

  def wombat_obj
    {
      'id' => @opencart_order_id.to_s,
      'opencart_id' => @opencart_id.to_s,
      'source' => @source,
      'order_id' => @opencart_order_id.to_s,
      'email' => @email,
      'status' => @status,
      'shipping_method' => @shipping_method,
      'tracking' => @tracking,
      'shipped_at' => @shipped_at,
      'shipping_address' => @shipping_address,
      'billing_address' => @billing_address,
      'items' => Util.wombat_array(@line_items)
    }
  end

  def self.wombat_obj_from_order order
    {
      'id' => order['id'],
      'order_id' => order['id'],
      'email' => order['email'],
      'channel' => order['source'],
      'shipping_address' => order['shipping_address'],
      'billing_address' => order['billing_address'],
      'items' => order['line_items'],
      'totals' => order['totals']
    }
  end

end
