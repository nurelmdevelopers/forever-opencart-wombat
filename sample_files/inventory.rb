class Inventory

  attr_reader :opencart_id, :sku, :quantity

  def add_obj variant
    @sku = variant.sku
    @opencart_id = variant.opencart_id
    @opencart_parent_id = variant.opencart_parent_id
    @quantity = variant.quantity

    self
  end

  def add_wombat_obj wombat_inventory
    @sku = wombat_inventory['product_id']
    @quantity = wombat_inventory['quantity']
    unless wombat_inventory['opencart_id'].nil?
      @opencart_id = wombat_inventory['opencart_id']
    end

    self
  end

  def wombat_obj
    {
      'id' => @sku,
      'product_id' => @sku,
      'opencart_id' => @opencart_id,
      'opencart_parent_id' => @opencart_parent_id.to_s,
      'quantity' => @quantity
    }
  end

  def opencart_obj
    {
      'inventory_quantity' => @quantity
    }
  end
end
