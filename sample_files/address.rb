class Address

  def add_opencart_obj opencart_address
    return self if opencart_address.nil?

    @address1 = opencart_address['address1']
    @address2 = opencart_address['address2']
    @zipcode = opencart_address['zip']
    @city = opencart_address['city']
    @state = opencart_address['province']
    @country = opencart_address['country_code']
    @phone = opencart_address['phone']

    self
  end

  def add_wombat_obj wombat_address
    return self if wombat_address.nil?

    @address1 = wombat_address['address1']
    @address2 = wombat_address['address2']
    @zipcode = wombat_address['zipcode']
    @city = wombat_address['city']
    @state = wombat_address['state']
    @country = wombat_address['country']
    @phone = wombat_address['phone']

    self
  end

  def wombat_obj
    {
      'address1' => @address1.nil? ? "" : @address1,
      'address2' => @address2.nil? ? "" : @address2,
      'zipcode' => @zipcode.nil? ? "" : @zipcode,
      'city' => @city.nil? ? "" : @city,
      'state' => @state.nil? ? "" : @state,
      'country' => @country.nil? ? "" : @country,
      'phone' => @phone.nil? ? "" : @phone
    }
  end

  def opencart_obj
    {
      'address1' => @address1.nil? ? "" : @address1,
      'address2' => @address2.nil? ? "" : @address2,
      'zip' => @zipcode.nil? ? "" : @zipcode,
      'city' => @city.nil? ? "" : @city,
      'province' => @state.nil? ? "" : @state,
      'country' => @country.nil? ? "" : @country,
      'phone' => @phone.nil? ? "" : @phone
    }
  end

end
