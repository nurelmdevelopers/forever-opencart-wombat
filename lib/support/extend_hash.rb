class ::Hash
	def deep_value(*keys)
		v = self[keys.shift]

    while keys.length > 0
      return nil unless v.is_a? Hash
      v = v[keys.shift]
    end
    v
  end

  def deep_merge(second)
    merger = proc { |key, v1, v2| Hash === v1 && Hash === v2 ? v1.merge(v2, &merger) : Array === v1 && Array === v2 ? v1 | v2 : [:undefined, nil, :nil].include?(v2) ? v1 : v2 }
    self.merge(second.to_h, &merger)
  end
end
