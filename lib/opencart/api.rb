require 'json'
require 'rest-client'
require 'uri'

module Opencart
  class Api

    def initialize(host_url, api_key, request_id=nil, payload, config)
      @host_url = host_url
      @api_key = api_key
      @request_id = request_id
      @config = config
      @payload = payload
    end

    # Obtain all orders added to OpenCart since the given date
    # Returns an array of OpenCart::Order objects
    def get_orders_since(datetime)
      datetime = URI.escape(datetime.to_time.strftime("%Y-%m-%d %T"))
      url = @host_url + "/api/rest/orders/details/added_from/#{datetime}"

      response = RestClient.get("#{url}", { 'X-Oc-Restadmin-Id' => @api_key })
      json = JSON.parse(response.force_encoding("utf-8"))

      ## The following is messy. If no orders were found, the OpenCart API
      ## responds with an error code, even though that's not really an error.
      ## If their "No orders found" message changes, this will break
      if json['success']
        json['data'].map{ |order_json| Opencart::Order.new(order_json,@payload,@config) }
      elsif json['error'] == 'No orders found'
        {}
      else
        raise json['error']
      end
    end

  end
end
