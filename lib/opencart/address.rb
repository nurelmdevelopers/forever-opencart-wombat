module Opencart
  class Address
    def initialize(hash, type)
      @address_type = type

      @firstname = nil
      @lastname = nil
      @company = nil
      @address_1 = nil
      @address_2 = nil
      @postcode = nil
      @city = nil
      @zone = nil
      @country = nil
      @iso_code_3 = nil

      # Sample Opencart shipping address (from orders/details request)
        # "shipping_firstname": "Test",
        # "shipping_lastname": "User 2",
        # "shipping_company": "company",
        # "shipping_address_1": "Kossuth Lajos út 88",
        # "shipping_address_2": "test",
        # "shipping_postcode": "1111",
        # "shipping_city": "Budapest",
        # "shipping_zone_id": "1433",
        # "shipping_zone": "Budapest",
        # "shipping_zone_code": "BU",
        # "shipping_country_id": "97",
        # "shipping_country": "Hungary",
        # "shipping_iso_code_2": "HU",
        # "shipping_iso_code_3": "HUN",
        # "shipping_address_format": "",
        # "shipping_method": "Flat Shipping Rate",
        # "shipping_code": "flat.flat",

      # Sample Opencarft billing address (from orders/details request)
        # "payment_firstname": "Test",
        # "payment_lastname": "User 2",
        # "payment_company": "company",
        # "payment_company_id": "company",
        # "payment_tax_id": "",
        # "payment_address_1": "Kossuth Lajos út 88",
        # "payment_address_2": "test",
        # "payment_postcode": "1111",
        # "payment_city": "Budapest",
        # "payment_zone_id": "1433",
        # "payment_zone": "Budapest",
        # "payment_zone_code": "BU",
        # "payment_country_id": "97",
        # "payment_country": "Hungary",
        # "payment_iso_code_2": "HU",
        # "payment_iso_code_3": "HUN",
        # "payment_address_format": "",
        # "payment_method": "Cash On Delivery",
        # "payment_code": "cod",
    end
  end
end
