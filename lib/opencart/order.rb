module Opencart
  class Order
    STATUS_MAP = {
      "1" => "Pending",
      "2" => "Processing",
      "3" => "Shipped",
      "5" => "Complete",
      "7" => "Canceled",
      "8" => "Denied",
      "9" => "Canceled Reversal",
      "10" => "Failed",
      "11" => "Refunded",
      "12" => "Reversed",
      "13" => "Chargeback",
      "14" => "Expired",
      "15" => "Processed",
      "16" => "Voided"
    }

    def initialize(hash,payload,config)
      @payload = payload
      @config = config

      @original_hash = hash ### Store original hash for use in member methods

      @id = hash['order_id']
      @status_id = hash['order_status_id']
      @status_name = STATUS_MAP[@status_id]
      @store_name = hash['store_name']
      @store_url = hash['store_url']
      @firstname = hash['firstname']
      @lastname = hash['lastname']
      @email = hash['email']
      @telephone = hash['telephone']
      @total = hash['total'].to_f
      @invoice_no = hash['invoice_no']

      # Custom Logic for Forever client
      @forever_id = hash['forever_id']
      @partner_id = hash['erp_id']

      # A shipping address is required for an order to be successfully submitted to netsuite.
      # So if the shipping info is missing, the payment address/info is used.
      @shipping_address = {
        firstname: hash['shipping_firstname'].empty? ? hash['payment_firstname'] : hash['shipping_firstname'],
        lastname: hash['shipping_lastname'].empty? ? hash['payment_lastname'] : hash['shipping_lastname'],
        address_1: hash['shipping_address_1'].empty? ? hash['payment_address_1'] : hash['shipping_address_1'],
        address_2: hash['shipping_address_2'].empty? ? hash['payment_address_2'] : hash['shipping_address_2'],
        postcode: hash['shipping_postcode'].empty? ? hash['payment_postcode'] : hash['shipping_postcode'],      # Zipcode
        city: hash['shipping_city'].empty? ? hash['payment_city'] : hash['shipping_city'],    # City name
        zone: hash['shipping_zone'].empty? ? hash['payment_zone'] : hash['shipping_zone'],         # State name
        iso_code_3: hash['shipping_iso_code_3'].empty? ? hash['payment_iso_code_3'] : hash['shipping_iso_code_3'], # 3 letter Country ISO code
      }

      @payment_address = {
        firstname: hash['payment_firstname'],
        lastname: hash['payment_lastname'],
        address_1: hash['payment_address_1'],
        address_2: hash['payment_address_2'],
        postcode: hash['payment_postcode'],                     # Zipcode
        city: hash['payment_city'],                             # City name
        zone: hash['payment_zone'],                             # State name
        iso_code_3: hash['payment_iso_code_3']                  # 3 letter Country ISO code
      }

      @payment_method = hash['payment_method']

      @products = hash['products'].map do |p|
        ##TODO: Add actual product objects?
        {
          product_id: p['product_id'],
          name: p['name'],
          model: p['model'],
          quantity: p['quantity'].to_i,
          price: p['base_price'].to_f,
          option: p['option']
        }
      end

      @totals = hash['totals'].map do |t|
        {
          title: t['title'],
          text: t['text'],
          extra: t['extra']
        }
      end

      @currency_code = hash['currency_code']                    # Currency ISO code
      @date_added = hash['date_added'].to_time.utc.iso8601

      @custom_fields = {}

      # Check for custom field maps
      @custom_fields_map = @config[:custom_fields_map]
    end

    def as_hash
      instance_variables.map do |var|
        [var[1..-1].to_sym, instance_variable_get(var)]
      end.to_h
    end

    ### REMOVE THIS, it is for debugging
    def original_hash
      @original_hash.merge(:id => @id)
    end

    def as_wombat_hash
      wombat_hash =
      {
        id: @id,
        status: @status_name,
        channel: @store_url,
        email: @email,
        currency: @currency_code,
        placed_on: @date_added,
        totals: wombat_totals_hash,
        line_items: products_as_wombat_line_items,
        adjustments: wombat_adjustments_array,
        shipping_address: {
          firstname: @shipping_address[:firstname],
          lastname: @shipping_address[:lastname],
          address1: @shipping_address[:address_1],
          address2: @shipping_address[:address_2],
          zipcode: @shipping_address[:postcode],
          city: @shipping_address[:city],
          state: @shipping_address[:zone],
          country: @shipping_address[:iso_code_3],
          phone: ""                                      # A phone number is only specified for a customer not an address?
        },
        billing_address: {
          firstname: @payment_address[:firstname],
          lastname: @payment_address[:lastname],
          address1: @payment_address[:address_1],
          address2: @payment_address[:address_2],
          zipcode: @payment_address[:postcode],
          city: @payment_address[:city],
          state: @payment_address[:zone],
          country: @payment_address[:iso_code_3],
          phone: ""                                      # A phone number is only specified for a customer not an address?
        },
        payments: wombat_payments_array,
        custom_fields: wombat_custom_fields_hash
      }

      # Custom Logic for Forever client
      # Pass extra order fields within this netsuite_order_fields hash
      if @partner_id!="0" and @partner_id!=0 and @partner_id!=nil
        wombat_hash[:netsuite_order_fields] = {partner_id: @partner_id}
      end

      # Pass extra customer fields within this netsuite_customer_fields hash
      if @forever_id!="0" and @forever_id!=nil
        wombat_hash[:netsuite_customer_fields] = {title: @forever_id}
      end
      # END Custom Logic for Forever client

      wombat_hash
    end

    # Converts the OpenCart @products array of hashes to an array of line item hashes for Wombat
    def products_as_wombat_line_items
      options = []
      line_items = @products.map do |p|
        if !p[:option].blank?
          p[:option].each do |e|
            hash = e
            hash['parent_quantity'] = p[:quantity]
            options << hash
          end
        end
        {
          product_id: p[:model],
          name: p[:model],
          quantity: p[:quantity],
          price: p[:price]
        }
      end

      # Product options that are turned into line items on NetSuite Sales Orders
      option_line_items = options.map do |o|
        next if o['erp_id'].blank?
        per_item_quantity = o['name']=="Extra Pages" ? o['value'].to_i : 1
        quantity = per_item_quantity*o['parent_quantity']
        {
          product_id: o['erp_id'],
          name: o['name'],
          quantity: quantity,
          price: o['price'].to_f / per_item_quantity # price for an option is the total (price*per_item_quantity)
        }
      end
      option_line_items.compact!
      line_items += option_line_items if !option_line_items.blank?

      line_items
    end

    # Calculates the Shipping tax total by summing the "extra" attribute for
    # all total hashes with a title of "Shipping"
    def shipping_tax
      @shipping_tax ||= @totals.select{ |t| t[:title].downcase=="shipping" }.map do |v|
        if v[:extra].blank?
          0
        else
          v[:extra].gsub(/[^\d\.-]/,'').to_f
        end
      end.reduce(:+) || 0
    end

    # Generate a totals hash for Wombat
    def wombat_totals_hash
      # Iterate over totals array and attempt to sum each total to their respective categories
      # All select statements below will use case-insensitive searching

      # Convert the totals to an easier to use format with downcased titles and currency strings converted to numbers
      # gsub(/[^\d\.-]/,'').to_f
      # (e.g. array.new(["sub-total", 10.00], ["state tax", 5.00]))
      ez_totals = @totals.map{ |t| [t[:title].downcase, t[:text].gsub(/[^\d\.-]/,'').to_f] }

      # Any total with "Sub-Total" in the title will be summed into the Sub-Total category
      sub_total = ez_totals.select{ |k,v| k.include? "sub-total" }.map{ |k,v| v }.reduce(:+) || 0

      # Any total with "Tax" in the title will be summed into the Tax category
      tax = ez_totals.select{ |k,v| k.include? "tax" }.map{ |k,v| v }.reduce(:+) || 0

      # Any total with "Shipping" in the title will be summed into the Shipping category
      shipping = ez_totals.select{ |k,v| k.include? "shipping" }.map{ |k,v| v }.reduce(:+) || 0

      # Shipping tax array element is added to the the ez_totals array in order
      # to easily calculate the adjustment total below in a consistent manner
      ez_totals << ["shipping_tax", shipping_tax]

      # Any total with "Coupon", "Voucher", "Tax", "Shipping", "Shipping Tax",
      # or "Adjustment" in the title will be summed into the Adjustment category
      adjustment = ez_totals.select do |k,v|
        k.include?("coupon") or k.include?("voucher") or k.include? "shipping" or
          k.include? "tax" or k.include? "adjustment" or k.include? "shipping_tax"
      end.map{ |k,v| v }.reduce(:+) || 0

      # We are making the assumption that the total payments from OpenCart will always match the overall order total
      payment = @total

      # We may have made a few assumptions but we now have a complete totals hash to give to Wombat
      {
        item: sub_total,                    # [Number]     Total of price * quantity for all line items
        adjustment: adjustment,             # [Number]     Total of all adjustment values
        tax: tax,                           # [Number]     Total of tax adjustment values
        shipping: shipping,                 # [Number]     Total of shipping adjustment values
        shipping_tax: shipping_tax,         # [Number]     Total of shipping tax adjustment values
        payment: payment,                   # [Number]     Total of all payments for this order
        order: @total                       # [Number]     Overall total of order
      }
    end

    # Generate an adjustments array for Wombat
    def wombat_adjustments_array
      # Sample data from Wombat
      # "adjustments": [
      # {
      #   "name": "Tax",
      #   "value": 10
      # }]

      # Convert the totals to an easier to use format with currency strings converted to numbers
      # (e.g. array.new(["Sub-Total", 10.00], ["State Tax", 5.00]))
      ez_totals = @totals.map{ |t| [t[:title], t[:text].gsub(/[^\d\.-]/,'').to_f] }
      totals_hash = {tax: 0, shipping: 0, shipping_tax: shipping_tax, discount: 0}

      # Adjustments should include all Coupons, Vouchers, Shipping, and Tax adjustments
      ez_totals.each do |k,v|
        case k
        when /coupon/i
          totals_hash[:discount] += v
        when /voucher/i
          totals_hash[:discount] += v
        when /shipping/i
          totals_hash[:shipping] += v
        when /tax/i
          totals_hash[:tax] += v
        end
      end

      totals_array = []

      totals_hash.each do |key, value|
        totals_array << { 'name' => key.to_s.capitalize, 'value' => value} if value != 0
      end

      totals_array
    end

    # Generate a payments array for Wombat
    def wombat_payments_array
      # Sample data from Wombat
      # "payments": [
      # {
      #   "number": 63,
      #   "status": "completed",
      #   "amount": 210,
      #   "payment_method": "Credit Card"
      # }]

      # OpenCart does not appear to supply very much information about payments in the API
      # Therefore, we are assuming that there will always be a single payment with an amount
      # that is equal to the order total
      [{
        number: @invoice_no,                  # Assume the payment number is the invoice number
        status: "",                           # OpenCart does not supply this value
        amount: @total,                       # Assume the payment amount is equal to the order total
        payment_method: @payment_method       # OpenCart specifies this as a string
      }]
    end

    # Generate a custom fields hash for Wombat
    def wombat_custom_fields_hash
      cf_map = JSON.parse(@custom_fields_map)[0] rescue nil
      return { error: "no custom field map" } unless cf_map.is_a? Hash

      custom_fields_hash = {}

      cf_map.each do |k,v|
        from = k.split(";;")

        value = @original_hash.deep_value(*from)

        # if v = "key1;;subkey1;;2ndsubkey1||key2;;subkey2||key3" it will map to:
        #
        # "custom_fields" => {
        #   "key1" => { "subkey1" => { "2ndsubkey1" => value }},
        #   "key2" => { "subkey2" => value }
        #   "key3" => value
        # }
        v.split('||').each do |i| # Map value to specified hash structure(s)
          to = i.split(';;')
          new_hash = to.reverse.inject(value) { |a,n| { n=>a } }
          custom_fields_hash = custom_fields_hash.deep_merge(new_hash)
        end
      end

      custom_fields_hash
    end
  end
end
