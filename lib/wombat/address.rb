module Wombat
  class Address
    # This class is not used now but it is left to convey organizational structure
    # in the case where Wombat may be used to push orders or other entities to OpenCart in the future
    def initialize(hash)
      # All nil and otherwise empty values should be filled by parsing the passed hash
      @firstname = nil                      # [String]     First name of the recipient
      @lastname = nil                       # [String]     Last name of the recipient
      @address1 = nil                       # [String]     First line of address
      @address2 = nil                       # [String]     Last line of address
      @zipcode = nil                        # [String]     Zipcode
      @city = nil                           # [String]     City name
      @state = nil                          # [String]     State name
      @country = nil                        # [String]     Country ISO code
      @phone = nil                          # [String]     Phone Number

      @address_type = nil # e.g. Shipping or Billing
    end
  end
end
