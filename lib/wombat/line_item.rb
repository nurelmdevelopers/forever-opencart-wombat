module Wombat
  class LineItem
    # This class is not used now but it is left to convey organizational structure
    # in the case where Wombat may be used to push orders or other entities to OpenCart in the future
    def initialize(hash)
      # All nil and otherwise empty values should be filled by parsing the passed hash
      @product_id = nil                     # [String]     Unique identifier of product
      @name = nil                           # [String]     Product's name
      @quantity = nil                       # [Number]     Quantity ordered
      @price = nil                          # [Number]     Price per item
    end
  end
end
