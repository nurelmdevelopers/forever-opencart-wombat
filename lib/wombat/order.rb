module Wombat
  class Order
    # This class is not used now but it is left to convey organizational structure
    # in the case where Wombat may be used to push orders or other entities to OpenCart in the future
    def initialize(hash)
      # All nil and otherwise empty values should be filled by parsing the passed hash
      @id = nil                             # [String]     Unique identifier for the order
      @status = nil                         # [String]     Current order status
      @channel = nil                        # [String]     Location where order was placed
      @email = nil                          # [String]     Customers email address
      @currency = nil                       # [String]     Currency ISO code
      @placed_on = nil                      # [String]     Date & Time order was placed (ISO format)

      @totals = {                           # [OrderTotal] Order value totals
        item: nil,                          # [Number]     Total of price * quantity for all line items
        adjustment: nil,                    # [Number]     Total of all adjustment values
        tax: nil,                           # [Number]     Total of tax adjustment values
        shipping: nil,                      # [Number]     Total of shipping adjustment values
        payment: nil,                       # [Number]     Total of all payments for this order
        order: nil                          # [Number]     Overall total of order
      },

      @line_items = []                      # [LineItem Array]     Array of the order's line items
      @adjustments = []                     # [Adjustment Array]   Array of the order's adjustments
      @shipping_address = {}                # [Address]            Customer's shipping address
      @billing_address = {}                 # [Address]            Customer's billing address
      @payments = []                        # [Payment Array]      Array of the order's payments
    end
  end
end
