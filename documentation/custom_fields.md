Support has been added to the Wombat OpenCart integration in order to allow passing arbitrary custom field data from OpenCart to Wombat. This requires the configuration of a single Wombat parameter for each custom field that you want to retrieve.

## Wombat Configuration
The Wombat webhook "Receive Orders from OpenCart" should have a configurations parameter called "Custom Fields Map" of the "Mappings" type. In the actual POST request, this field will appear under the "parameters" section as "custom_fields_map".

Each key value pair of this Custom Fields Map is used to pair a single OpenCart field (the key) to one or more fields in the response object. In order to handle a nested field structure, each key and value pair can specify a tree of fields by deliminating with ";;". Additionally, a single OpenCart field may be mapped to multiple fields in the response object by deliminating with "||". See the code block below for examples.

### Example Mapping Values
	custom_fields_map: {
		"purchase_details;order_id" => "netsuite_custbody;;custbody_order_id||amazon;;opencart_order_id"
	}

	OpenCart field data
		{
			...
			sample_field: "value",
			purchase_details: {
				...
				order_id: "5000",
				...
			}
		}

	Response Object
		"orders": [
			...
			{
				...
				"custom_fields": {
					"netsuite_custbody": {
						"custbody_order_id": "5000"
					},
					"amazon": {
						"opencart_order_id": "5000"
					}
				}
				...
			},
			...
		]
